//
//  CheckInAnnotation.swift
//  BI-IOS
//
//  Created by Dagy Tran on 21/12/2017.
//  Copyright © 2017 Dagy Tran. All rights reserved.
//

import Foundation
import MapKit

class CheckInAnnotation: MKPointAnnotation {
    var checkIn: CheckIn
    init(checkIn: CheckIn) {
        self.checkIn = checkIn
        super.init()
        self.coordinate = CLLocationCoordinate2D(latitude: checkIn.lat, longitude: checkIn.lon)
        self.title = checkIn.credentials?.username ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm"
        self.subtitle = dateFormatter.string(from: checkIn.time)
    }
}
