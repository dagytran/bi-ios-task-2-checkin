//
//  Credentials.swift
//  BI-IOS
//
//  Created by Dagy Tran on 19/12/2017.
//  Copyright © 2017 Dagy Tran. All rights reserved.
//

import Foundation

enum Gender: String {
    case male
    case female
    case unknown
}

let Genders: [Gender] = [Gender.female, Gender.male, Gender.unknown]

struct Credentials {
    var username: String?
    var gender: Gender
    
    static let usernameKey = "username"
    static let genderKey = "gender"
}
