//
//  CheckIn.swift
//  BI-IOS
//
//  Created by Dagy Tran on 19/12/2017.
//  Copyright © 2017 Dagy Tran. All rights reserved.
//

import Foundation
import CoreLocation

class CheckIn {
    var key: String?
    var time: Date
    var lat: Double
    var lon: Double
    var credentials: Credentials?
    
    init(username: String?, gender: Gender, lat: Double, lon: Double, time: Date?) {
        self.credentials = Credentials(username: username, gender: gender)
        self.lat = lat
        self.lon = lon
        self.time = time ?? Date()
    }
    
    var coordinate2D: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: lat, longitude: lon)
        }
    }
    
    var serialized: [String: Any] {
        get {
            var dict: [String: Any] = ["time": time.timeIntervalSince1970, "lat": lat, "lon": lon]
            
            if let username = credentials?.username {
                dict["username"] = username
            }
            if let gender =  credentials?.gender.rawValue {
                dict["gender"] = gender
            }
            return dict
        }
    }
    
    static func deserialize(dict: [String: Any]) -> CheckIn? {
        guard let timestamp = dict["time"] as? Double,
        let lat = dict["lat"] as? Double,
        let lon = dict["lon"] as? Double
        else {return nil}
        
        let username = dict["username"] as? String
        var gender = Gender.unknown
        if let genderVal = dict["gender"] as? String, let genderB = Gender(rawValue: genderVal){
            gender = genderB
        }
        
        return CheckIn(username: username, gender: gender, lat: lat, lon: lon, time: Date(timeIntervalSince1970: timestamp))
    }
}
