//
//  CheckInMapViewModel.swift
//  BI-IOS
//
//  Created by Dagy Tran on 19/12/2017.
//  Copyright © 2017 Dagy Tran. All rights reserved.
//

import Foundation
import FirebaseDatabase
import MapKit

protocol CheckInViewModelling {
    var delegate: CheckInViewModelDelegate? {get set}
    var checkIns: [CheckIn] {get}
    var annotations: [CheckInAnnotation] {get}
    func addCheckIn(checkIn: CheckIn)
    func saveCredentials(username: String, gender: Gender?)
    func loadCredentials() -> Credentials?
    var credentials: Credentials? {get set}

}

protocol CheckInViewModelDelegate {
    func checkInAdded(checkIn: CheckIn)
}

class CheckInViewModel: CheckInViewModelling {
    
    // MARK: Stored values
    var APIService: DataServicing!
    var checkIns: [CheckIn] = []
    var credentials: Credentials?
    let genders: [Gender] = [Gender.unknown, Gender.female, Gender.male]
    
    var delegate: CheckInViewModelDelegate?
    
    // MARK: Computed values
    var annotations: [CheckInAnnotation] {
        get {
            let annotations = checkIns.map { (checkIn) -> CheckInAnnotation in
                return CheckInAnnotation(checkIn: checkIn)
            }
            
            if annotations.count > 0 {
                return annotations
            } else {
                return []
            }
        }
    }
    
    var username: String? {
        get {
            return credentials?.username
        }
        set {
            if credentials == nil {
                credentials = Credentials(username: username, gender: Gender.unknown)
            }
            credentials?.username = username
        }
    }
    
    var gender: Gender {
        get {
            return credentials?.gender ?? Gender.unknown
        }
        set {
            if credentials == nil {
                credentials = Credentials(username: "username", gender: gender)
            }
            credentials?.gender = gender
        }
    }
    
    // MARK: Life Cycle

    init() {
        APIService = DataService.shared
        APIService.observe(.childAdded) { [weak self] snapshot in
            guard let `self` = self else { return }
            if let val = snapshot.value as? [String: Any], let checkIn = CheckIn.deserialize(dict: val) {
                checkIn.key = snapshot.key
                self.checkIns.append(checkIn)
                self.delegate?.checkInAdded(checkIn: checkIn)
            }
        }
        credentials = loadCredentials()
    }
    
    deinit {
        APIService.removeAllObservers()
    }
    
    // MARK: Functions

    func addCheckIn(checkIn: CheckIn) {
        APIService.addCheckIn(checkIn)
    }
    
    func saveCredentials(username: String, gender: Gender?) {
        let defaults = UserDefaults.standard
        defaults.set(username, forKey: Credentials.usernameKey)
        if let gender = gender {
            defaults.set(gender.rawValue, forKey: Credentials.genderKey)
        }
    }
    
    func loadCredentials() -> Credentials? {
        let defaults = UserDefaults.standard
        guard let username = defaults.string(forKey: Credentials.usernameKey) else {return nil}
        
        let gender = defaults.string(forKey: Credentials.genderKey)
        return Credentials(username: username, gender: Gender(rawValue: gender ?? "unknown") ?? Gender.unknown)
    }
    
}
