//
//  ViewController.swift
//  BI-IOS
//
//  Created by Dagy Tran on 19/12/2017.
//  Copyright © 2017 Dagy Tran. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SnapKit

class CheckInMapViewController: UIViewController {
    
    var viewModel: CheckInViewModelling = CheckInViewModel()
    private var checkInsObserver: NSKeyValueObservation?
    weak var alertController: UIAlertController?
    
    weak var mapView: MKMapView!
    weak var addButton: UIButton!
    weak var centerButton: UIButton!
    
    private var locationManager: CLLocationManager!
    
    override func loadView() {
        super.loadView()
        
        view.backgroundColor = .white
        let mapView = MKMapView()
        view.addSubview(mapView)
        mapView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        self.mapView = mapView
        
        let addButton = UIButton(type: .custom)
        addButton.setTitle("+", for: .normal)
        addButton.setTitleColor(.white, for: .normal)
        addButton.titleLabel?.font = UIFont.systemFont(ofSize: 36, weight: .light)
        addButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 6, right: 0)
        addButton.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.2666666667, blue: 0.2156862745, alpha: 1)
        addButton.layer.cornerRadius = 22
        addButton.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
        addButton.layer.shadowOffset = CGSize(width: 0, height: 4)
        addButton.layer.shadowRadius = 12
        addButton.layer.shadowOpacity = 1
        view.addSubview(addButton)
        addButton.snp.makeConstraints { make in
            make.width.height.equalTo(44)
            make.bottom.right.equalToSuperview().inset(24)
        }
        self.addButton = addButton
        
        let centerButton = UIButton(type: .custom)
        centerButton.setImage(#imageLiteral(resourceName: "center").withRenderingMode(.alwaysTemplate), for: .normal)
        centerButton.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        centerButton.tintColor = #colorLiteral(red: 0.2615269721, green: 0.521964252, blue: 0.9553313851, alpha: 1)
        centerButton.backgroundColor = UIColor.white
        centerButton.layer.cornerRadius = 22
        centerButton.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
        centerButton.layer.shadowOffset = CGSize(width: 0, height: 4)
        centerButton.layer.shadowRadius = 12
        centerButton.layer.shadowOpacity = 1
        view.addSubview(centerButton)
        centerButton.snp.makeConstraints { make in
            make.width.height.equalTo(44)
            make.centerX.equalTo(addButton)
            make.bottom.equalTo(addButton.snp.top).offset(-12)
        }
        self.centerButton = centerButton
        
        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
        view.addSubview(blurView)
        blurView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(20)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        print(viewModel.checkIns)
        
        addButton.addTarget(self, action: #selector(addButtonTapped(_:)), for: .touchUpInside)
        centerButton.addTarget(self, action: #selector(centerButtonTapped(_:)), for: .touchUpInside)
        mapView.delegate = self
        viewModel.delegate = self
        refreshAnnotations()
    }
    
    func refreshAnnotations() {
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotations(viewModel.annotations)
    }
    
    deinit {
        checkInsObserver = nil
    }
    
    @objc
    func addButtonTapped(_ sender: UIButton) {
        presentCheckInAlert()
    }
    
    @objc
    func centerButtonTapped(_ sender: UIButton) {
        if let coordinate = locationManager.location?.coordinate {
            mapView.setCenter(coordinate, animated: true)
            let span = 0.01
            let region = MKCoordinateRegion(center: coordinate, span: MKCoordinateSpan(latitudeDelta: span, longitudeDelta: span))
            mapView.setRegion(region, animated: true)
        }
    }
    
    private func presentCheckInAlert() {
        let alertController = createAlertController()
        self.alertController = alertController
        present(alertController, animated: true, completion: nil)
    }
    
    @objc
    func donePicker() {
        self.alertController?.view.endEditing(true)
    }
    
    private func addCheckIn(username: String, gender: Gender) {
        guard let coordinate = locationManager.location?.coordinate else {return}
        
        let checkIn = CheckIn(username: username, gender: gender, lat: coordinate.latitude, lon: coordinate.longitude, time: Date())
        viewModel.addCheckIn(checkIn: checkIn)
    }
    
    private func createAlertController() -> UIAlertController {
        let alertController = UIAlertController(title: "New CheckIn", message: "Add a new CheckIn", preferredStyle: .alert)

        alertController.addTextField { [weak self] (textField) in
            textField.placeholder = "User Name"
            guard let `self` = self else { return }
            textField.text = self.viewModel.credentials?.username
        }
        alertController.addTextField { [weak self] (textField) in
            textField.placeholder = "Gender"
            guard let `self` = self else { return }
            let toolbar = UIToolbar()
            toolbar.barStyle = .default
            toolbar.sizeToFit()
            toolbar.setItems([UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.donePicker))], animated: true)
            
            textField.inputAccessoryView = toolbar
            let picker = UIPickerView()
            picker.delegate = self
            picker.dataSource = self
            textField.inputView = picker
            textField.text = self.viewModel.credentials?.gender.rawValue
            
        }
        
        alertController.view.backgroundColor = .white
        alertController.view.layer.cornerRadius = 2
        
        let addAction = UIAlertAction(title: "Add", style: .default) { [weak self] _ in
            guard let `self` = self else { return }
            let username = alertController.textFields?[0].text ?? ""
            let gender = Gender(rawValue: alertController.textFields?[1].text?.lowercased() ??  Gender.unknown.rawValue ) ?? Gender.unknown
            self.viewModel.saveCredentials(username: username, gender: gender)
            self.addCheckIn(username: username, gender: gender)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { [weak self] _ in
            guard let `self` = self else { return }
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(addAction)
        alertController.addAction(cancelAction)
        alertController.preferredAction = addAction

        return alertController
    }
}

extension CheckInMapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .follow
        }
    }
}

extension CheckInMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "annotation"
        
        guard let checkInAnnotation = annotation as? CheckInAnnotation else {return nil}
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier) ?? MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
        annotationView.canShowCallout = true
        
        if let gender = checkInAnnotation.checkIn.credentials?.gender {
            switch gender {
            case .male:
                annotationView.image = #imageLiteral(resourceName: "male")
            case .female:
                annotationView.image = #imageLiteral(resourceName: "female")
            default:
                annotationView.image = #imageLiteral(resourceName: "unknown")
            }
        } else {
            annotationView.image = #imageLiteral(resourceName: "unknown")
        }

        return annotationView
    }
}

extension CheckInMapViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let gender: Gender = row < Genders.count ? Genders[row] : Gender.unknown
        return gender.rawValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if viewModel.credentials == nil {
            viewModel.credentials = Credentials(username: "", gender: Gender.unknown)
        }
        let gender = row < Genders.count ? Genders[row] : Gender.unknown
        viewModel.credentials?.gender = gender
        self.alertController?.textFields?[1].text = gender.rawValue
    }
}

extension CheckInMapViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
}

extension CheckInMapViewController: CheckInViewModelDelegate {
    func checkInAdded(checkIn: CheckIn) {
        refreshAnnotations()
    }
}
