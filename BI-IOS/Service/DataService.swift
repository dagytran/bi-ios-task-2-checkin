//
//  DataService.swift
//  BI-IOS
//
//  Created by Dagy Tran on 19/12/2017.
//  Copyright © 2017 Dagy Tran. All rights reserved.
//

import Foundation
import FirebaseDatabase

protocol DataServicing {
    func removeAllObservers()
    func observe(_ eventType: DataEventType, with: @escaping (DataSnapshot) -> Void)
    func addCheckIn(_ checkIn: CheckIn)
    static var shared: DataServicing {get}
}


class DataService: DataServicing {
    static let shared: DataServicing = DataService()
    private var databaseReference: DatabaseReference
    
    init() {
        databaseReference = Database.database().reference().child("checkins")
    }
    
    func addCheckIn(_ checkIn: CheckIn) {
        let newCheckIn = databaseReference.childByAutoId()
        checkIn.key = newCheckIn.key
        newCheckIn.setValue(checkIn.serialized)
    }
    
    func observe(_ eventType: DataEventType, with: @escaping (DataSnapshot) -> Void) {
        databaseReference.observe(eventType, with: with)
    }
    
    func removeAllObservers() {
        databaseReference.removeAllObservers()
    }
    
}
